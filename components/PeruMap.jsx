import { useState, useEffect } from 'react'
import { feature } from 'topojson-client'
import { geoEqualEarth, geoPath } from 'd3-geo'

import styles from '../styles/PeruMap.module.css'
import peruMapa from '../public/maps/peru.json'

const projection = geoEqualEarth().rotate([72, 7, 0]).scale(1700)

const PeruMap = () => {
    const [geoFile, setGeoFile] = useState([])

    useEffect(()=>{
        const objMapa = feature(peruMapa, peruMapa.objects["peru"]).features
        
        setGeoFile(objMapa)
    }, [])

    

    return <>
        <div className={styles.container} data-tip=''>
            <svg viewBox="0 0 1000 646">
                <g className='peru'>
                    {
                        geoFile.map((d, i)=>{
                           return <path 
                                d={geoPath().projection(projection)(d)}
                                key={`geoPath-${i}`}
                                fill='#00778C'
                                stroke='#FFFFFF'
                                strokeWidth={0.5}
                            />
                        })
                    }
                </g>
            </svg>
        </div>
    </>
}

export default PeruMap