import React, { PureComponent } from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';

import styles from '../styles/ProgressChart.module.css'


export default ({dataset, title}) => {
    return (
        <div className={styles.containerChart}>
            <ResponsiveContainer width="100%" height="100%">
                <AreaChart
                    width={500}
                    height={400}
                    data={dataset}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                <XAxis dataKey="name" />
                <YAxis 
                    interval='preserveStartEnd'
                    width={100}
                    scale='linear'
                    domain={[0, 'dataMax + 5000']} />
                <Tooltip />
                <Area type="monotone" 
                    fill='#d2effd'
                    dataKey="value" 
                    stroke="#8884d8" 
                    fill="#8884d8" />
                </AreaChart>
            </ResponsiveContainer>
            <h4>{title}</h4>
        </div>
    )

}
