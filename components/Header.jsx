import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default ()=> {
    return (
      <header className={styles.header}>
        <div className={styles.logo}>
            <Link href="/">
                <a>
                    <h2>Open Covid - Perú</h2>
                </a>
            </Link>
        </div>
        <div className={styles.rightMenu}>
          <nav className={styles.navbar}>
            <ul>
              <li>
              <Link href="/">
                <a>
                  Inicio
                </a>
              </Link>
                <a href="#">
                  Informacion Relevante
                </a>
              </li>
            </ul>
          </nav>
          <div className={styles.language}>
            <select className={styles.buttonLanguage} href="#">
              <option>Español
              </option>
              <option>Quechua
              </option>
              <option>Aymara
              </option>
            </select>
          </div>
        </div>
      </header>

    )

}