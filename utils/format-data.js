import fs from 'fs'

/**
 * Take data files and process json for chart model type
 * Type: Array of { name: string, value: number }
 */
export default function formatChartData () {
    const mapValue = fs.readFileSync("./public/data/histPositivos.json", { encoding: 'utf8' })
    let arrFechas = JSON.parse(mapValue)

    let dataset = {
        positivos:[],
        positivosDiarios:[]
    }
    const year = (s) => s.slice(0, 4)
    const month = (s) => s.slice(4, 6)
    const day = (s) => s.slice(6, 8)

    const matrixMap = (v) => [year(v), month(v), day(v)]
    arrFechas = arrFechas.sort((a, b) => {
        let aa = a.fechaResultado
        let bb = b.fechaResultado
        aa = matrixMap(aa).join("-")
        bb = matrixMap(bb).join("-")
        return new Date(aa).getTime() - new Date(bb).getTime()
    })

    let acumuladorPositivos = 0
    arrFechas.forEach(element=>{

        const nombre = element.fechaResultado
        let nombreFinal = matrixMap(nombre)
        let nameDataSet = [nombreFinal[2],nombreFinal[1]].join("/")
        
        const positivosCovid = element.departamentos.find(({departamento})=>departamento==="TOTAL").positivos
        acumuladorPositivos += positivosCovid
        dataset.positivos.push({
            name: nameDataSet,
            value: acumuladorPositivos
        })

        dataset.positivosDiarios.push({
            name: nameDataSet,
            value: positivosCovid
        })

    })
    
    return dataset
}