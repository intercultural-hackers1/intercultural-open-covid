
# Intercultural Hackers - Equipo 8

Nosotros, INTERCULTURAL HACKERS buscamos crear una plataforma que se ajuste a las necesidades del usuario desde un enfoque positivo y fácil de utilizar por los ciudadanos. Asimismo, considerando la multiculturalidad de nuestro país siendo adaptable a las 3 principales lenguas (español, quechua y aymara). Para implementar esto, la propuesta se basa en un sitio web diseñado en el framework Next con lenguaje Java Script, utilizando el hosting Vercel y bases de datos en formato Json para un acceso más eficiente y rápido. 

![Dashboard](https://gitlab.com/intercultural-hackers1/intercultural-open-covid/-/raw/master/public/screenshot.png)


[LINK DEL SITIO WEB](https://intercultural-hackers.vercel.app/)


##  ¿Cómo lo ejecuto en local? 

Para levantar el proyecto de manera local debes ejecutar los siguientes comandos en la raíz del proyecto:

```
npm install # instalar las dependencias
npm run dev # levantar el entorno de desarrollo
```

Abrir [http://localhost:3000](http://localhost:3000) en tu navegador para ver la aplicacion ejecutada.

## Arquitectura

El lenguaje de programación es Javascript utilizando Next JS como framework,el cual permite utilizar React en el lado del servidor, asimismo el servicio en la nube es Vercel, el cual ofrece una gran compatibilidad con Next y Gitlab. Asimismo para la descarga de informacion utilizamos Node, el cual tiene el npm script ya listo para la descarga y la transformación de las diversas fuentes de datos a formatos json. Esto permite un acceso rápido a la información por parte del usuario final.

![Tecnologias](https://gitlab.com/intercultural-hackers1/intercultural-open-covid/-/raw/master/public/tech.png)




