
const csvToJson = require("convert-csv-to-json");

module.exports = async function transformCsvToJson(csvFileName) {
  const json = csvToJson.fieldDelimiter(';').getJsonFromCsv(`./public/data/${csvFileName}`); //XLSX.utils.sheet_to_json(sheet)

  const jsonFormat = json.map((element) => {
    const {
      DEPARTAMENTO: departamento,
      FECHA_FALLECIMIENTO: fechaFallecimiento
    } = element;
    return {
      departamento,
      fechaFallecimiento
    };
  });

  let jsonStatus = [];
  let jsonHist = [];
  let registros = jsonFormat.length;

  jsonFormat.forEach((element) => {
    let objDepartamento = element.departamento;
    let objFechaFallecimiento = element.fechaFallecimiento;

    let indice = jsonStatus.findIndex(
      ({ departamento }) => departamento === objDepartamento
    );

    let indiceHist = jsonHist.findIndex(
      ({ fechaFallecimiento }) => fechaFallecimiento === objFechaFallecimiento
    );

    if (indice < 0) {
      jsonStatus.push({
        departamento: objDepartamento,
        cantidad: 1
      });
    } else {
      jsonStatus[indice].cantidad += 1;
    }

    if (indiceHist < 0) {
      jsonHist.push({
        fechaFallecimiento: objFechaFallecimiento,
        departamentos: [{ 
                          departamento: objDepartamento, 
                          cantidad: 1,
                      }],
      });
    } else {
      
      let subIndiceHist = jsonHist[indiceHist].departamentos.findIndex(
        ({ departamento }) => departamento === objDepartamento
      );
      if (subIndiceHist < 0) {
        jsonHist[indiceHist].departamentos.push({
          departamento: objDepartamento,
          cantidad: 1
        });
      } else {
          jsonHist[indiceHist].departamentos[subIndiceHist].cantidad += 1;
      }
    }
  });

  let totalCantidad = 0;
  jsonStatus.forEach((elemento, i) => {
    totalCantidad += elemento.cantidad
  });
  jsonStatus.push({
    departamento: "TOTAL",
    cantidad: totalCantidad
  });

  jsonHist.forEach((element, i) => {
      let totalCantidad = element.departamentos.reduce((acc, cur) => {
          return acc + cur.cantidad;
      }, 0);
      jsonHist[i].departamentos.push({
          departamento: "TOTAL",
          cantidad: totalCantidad
      });
  });
  

  return {
    jsonStatus,
    jsonHist,
    registros
  };
};
