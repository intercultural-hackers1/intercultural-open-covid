const download = require('download')
const fs = require('fs-extra')
const transformCsvToJsonPositivos = require('./transform-csv-to-json-positivos')
const transformCsvToJsonFallecidos = require('./transform-csv-to-json-fallecidos')
const transformCsvToJsonCamasUCI = require('./transform-csv-to-json-camas-uci')

const urlPositivos = 'https://cloud.minsa.gob.pe/s/Y8w3wHsEdYQSZRp/download'
const urlFallecidos = 'https://cloud.minsa.gob.pe/s/Md37cjXmjT9qYSa/download'
const urlCamasUCI = 'http://datos.susalud.gob.pe/node/223/download'

const fileNamePositivos = 'positivos_covid.csv'
const fileNameFallecidos = 'fallecidos_covid.csv'
const fileNameCamasUCI = 'download.csv'

download( urlPositivos, 'public/data', { fileNamePositivos } )
  .then(async () => {
    const json = await transformCsvToJsonPositivos(fileNamePositivos)
    if(json.jsonStatus.length>0){
      await fs.writeJson(`./public/data/latestPositivos.json`, json.jsonStatus)
      await fs.writeJson('./public/data/histPositivos.json', json.jsonHist)
      await fs.writeJson('./public/data/ultimoCorte.json', { fechaCorte: json.fechaCorte, rows: json.registros })
    }
    await fs.unlink('public/data/'+fileNamePositivos, function (err) {
      if (err) throw err;
      console.log('File deleted!');
    });
  })
  .catch(err => {
    console.error(`${urlPositivos} can't be downloaded. Error:`)
    console.error(err)
  })

download(urlFallecidos, 'public/data', {fileNameFallecidos})
  .then(async ()=>{
    const json = await transformCsvToJsonFallecidos(fileNameFallecidos)
    if(json.jsonStatus.length>0){
      await fs.writeJson(`./public/data/latestFallecidos.json`, json.jsonStatus)
      await fs.writeJson('./public/data/histFallecidos.json', json.jsonHist)
    }
    await fs.unlink('public/data/'+fileNameFallecidos, function (err) {
        if (err) throw err;
        console.log('File deleted!');
    });
  })
  .catch(err => {
    console.error(`${urlFallecidos} can't be downloaded. Error:`)
    console.error(err)
  })
 
/* download(urlCamasUCI, 'public/data', {fileNameCamasUCI})
  .then(async ()=>{
    console.log("download")
    const json = await transformCsvToJsonCamasUCI(fileNameCamasUCI)
    if(json.jsonStatus.length>0){
      await fs.writeJson(`./public/data/latestCamasUCI.json`, json.jsonStatus)
    }
    await fs.unlink('public/data/'+fileNameCamasUCI, function (err) {
        if (err) throw err;
        console.log('File deleted!');
    });
  })
  .catch(err => {
    console.error(`${urlCamasUCI} can't be downloaded. Error:`)
    console.error(err)
  }) */