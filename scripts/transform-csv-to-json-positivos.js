
const csvToJson = require("convert-csv-to-json");

module.exports = async function transformCsvToJson(csvFileName) {
  const json = csvToJson.fieldDelimiter(';').getJsonFromCsv(`./public/data/${csvFileName}`); //XLSX.utils.sheet_to_json(sheet)

  const jsonFormat = json.map((element) => {
    const {
      DEPARTAMENTO: departamento,
      FECHA_RESULTADO: fechaResultado
    } = element;
    return {
      departamento,
      fechaResultado
    };
  });

  let jsonStatus = []
  let jsonHist = []
  let registros = jsonFormat.length
  let fechaCorte = +new Date();

  jsonFormat.forEach((element) => {
    let objDepartamento = element.departamento;
    let objFechaResultado = element.fechaResultado;

    let indice = jsonStatus.findIndex(
      ({ departamento }) => departamento === objDepartamento
    );

    let indiceHist = jsonHist.findIndex(
      ({ fechaResultado }) => fechaResultado === objFechaResultado
    );

    if (indice < 0) {
      jsonStatus.push({
        departamento: objDepartamento,
        positivos: 1
      });
    } else {
      jsonStatus[indice].positivos += 1;
    }

    if (indiceHist < 0) {
      jsonHist.push({
        fechaResultado: objFechaResultado,
        departamentos: [{ 
                          departamento: objDepartamento, 
                          positivos: 1,
                      }],
      });
    } else {
      
      let subIndiceHist = jsonHist[indiceHist].departamentos.findIndex(
        ({ departamento }) => departamento === objDepartamento
      );
      if (subIndiceHist < 0) {
        jsonHist[indiceHist].departamentos.push({
          departamento: objDepartamento,
          positivos: 1
        });
      } else {
          jsonHist[indiceHist].departamentos[subIndiceHist].positivos += 1;
      }
    }
  });

  let totalPositivos = 0;
  jsonStatus.forEach((elemento, i) => {
      totalPositivos += elemento.positivos
  });

  jsonStatus.push({
    departamento: "TOTAL",
    positivos: totalPositivos
  });

  jsonHist.forEach((element, i) => {
      let totalPositivos = element.departamentos.reduce((acc, cur) => {
          return acc + cur.positivos;
      }, 0);
      jsonHist[i].departamentos.push({
          departamento: "TOTAL",
          positivos: totalPositivos
      });
  });

  return {
    jsonStatus,
    fechaCorte,
    jsonHist,
    registros
  }
};
