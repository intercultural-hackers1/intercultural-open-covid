import Head from 'next/head'
import {useMemo, useState} from 'react'
import Link from 'next/link'

import ProgressChart from '../components/ProgressChart'
import formatChartData from '../utils/format-data'
import FormatNumber from '../components/FormatNumber'
import styles from '../styles/Home.module.css'
import Header from "../components/Header";
export default function Home({dataPositivos, histPositivos, dataFallecidos, dataChart}) {
  const [chat, setChat] = useState(false)
  
  const totalsPositivos = useMemo(()=>{
    return dataPositivos.find(dp=>dp.departamento==='TOTAL')}, 
  [dataPositivos])
  const totalsFallecidos = useMemo(()=>{
    return dataFallecidos.find(dp=>dp.departamento==='TOTAL')}, 
  [dataFallecidos])
  
  const casosNuevos = useMemo(()=>{
    const year = (s) => s.slice(0, 4)
    const month = (s) => s.slice(4, 6)
    const day = (s) => s.slice(6, 8)

    const matrixMap = (v) => [year(v), month(v), day(v)]
    const nuevoHist = histPositivos.slice()
    nuevoHist.sort((a, b) => {
        let aa = a.fechaResultado
        let bb = b.fechaResultado
        aa = matrixMap(aa).join("-")
        bb = matrixMap(bb).join("-")
        return new Date(aa).getTime() - new Date(bb).getTime()
    })
    
    return nuevoHist[nuevoHist.length-1].departamentos.find(d=>d.departamento==='TOTAL').positivos
  }, [histPositivos])
  return (
    <>
      <Head>
        <title>Intercultural Hackers</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />
      <main className={styles.main}>
        <section className={styles.search}>
          <div className={styles.containerSearchButton}>
            <div className={styles.imageLocation}>
              <img src="/Ubicacion.svg" width={24} alt="..." />
            </div>
            <input
              className={styles.searchInput} 
              type="search" 
              placeholder="Buscar Infornacion por Localidad" />
            <button className={styles.searchButton}>
              <img width={22} src="/Lupa.svg" alt="..." />
            </button>
          </div>
          <Link href="/semaforo">
            <a className={styles.button}>
              Ver Semáforo Epidemiológico
            </a>
          </Link>
        </section>
        <div className={styles.hr}>

        </div>
        <section className={styles.containerCards}>
          <div className={styles.timeAgo}>
            <strong>Datos Actualizados al: </strong> 08/02/2021 
          </div>
          <div className={styles.grid}>
            <div className={styles.card}>
              <div className={styles.cardText}>
                <div className={styles.cardTitle}>
                  <img src="CasosRecuperados.svg" width="30px" />
                  <h5>Casos Recuperados</h5>
                </div>
                <div className={styles.cardDescripcion}>
                  <p>2.345.346</p>
                </div>
                <div className={styles.cardTitle}>
                  <img src="CasosActivos.svg" width="30px" />
                  <h5><small>Casos Hospitalizados</small></h5>
                </div>
                <div className={styles.cardDescripcion}>
                  <p><small>345.346</small></p>
                </div>
              </div>
             
            </div>

            <div className={styles.card}>
              <div className={styles.cardText}>
                <div className={styles.cardTitle}>
                  <h5>Casos Confirmados</h5>
                </div>
                <div className={styles.cardDescripcion}>
                  <p><small>
                    <FormatNumber>
                      {totalsPositivos.positivos}
                    </FormatNumber></small></p>
                  <p><small>Acumulado</small></p>
                </div>
                <div className={styles.cardDescripcion}>
                  <p><small>
                    <FormatNumber>
                      {casosNuevos}
                    </FormatNumber></small></p>
                  <p><small>Casos Nuevos</small></p>
                </div>
              </div>
              
            </div>
            
            <div className={styles.card}>
              <div className={styles.cardText}>
                <div className={styles.cardTitle}>
                  <h5>Fallecimiento</h5>
                </div>
                <div className={styles.cardDescripcion}>
                  <p>
                    <FormatNumber>
                      {totalsFallecidos.cantidad}
                    </FormatNumber>
                  </p>
                  <p><small>Acumulado</small></p>
                </div>
              </div>
             
            </div>

            <div className={styles.card}>
              <div className={styles.cardText}>
                <div className={styles.cardTitle}>
                  <h5>Personas vacunadas</h5>
                </div>
                <div className={styles.cardDescripcion}>
                  <p>318,702</p>
                  <p><small>Acumulado</small></p>
                </div>
              </div>
              
            </div>

          </div>
        </section>
        <section className={styles.charts}>
          <div class={styles.containerOptions}>
            <a className={styles.option}>
              <img src="CamasUci.svg" width={60} />
              <span>Camas UCI</span>
            </a>
            <a className={styles.option}>
              <img src="Oxigeno.svg" width={60} />
              <span>Oxígeno</span>
            </a>
            <a className={styles.option}>
              <img src="CamasCovid.svg" width={60} />
              <span>Camas Covid</span>
            </a>
          </div>
          <div className={styles.containerCharts}>
            <ProgressChart dataset={dataChart.positivos} title="Casos Totales" />
            <ProgressChart dataset={dataChart.positivosDiarios} title="Casos Diarios" />
            <ProgressChart dataset={dataChart.positivos} title="Casos Activos" />
            <ProgressChart dataset={dataChart.positivos} title="Casos Casos Confirmados vs Pruebas por Dia" />
          </div>
        </section>
      </main>
      {
        chat &&<div class={styles.chatWindow}>
          <div class={styles.chatHeader}>
            <h5>Open Covid-Perú</h5>1
            <button>-</button>
            <button onClick={()=>setChat(false)}>x</button>
          </div>
          <div class={styles.chatBody}>
          </div>
          <div class={styles.chatInput}>
            <input type="text" />
            <span>200</span>
            <span>Enviar</span>
          </div>
        </div>

      }
      
      <div 
        className={styles.chat}
        onClick={()=>setChat(false)}
      >
        <img src="Chat.svg" width={100} alt="..." />
      </div>
    </>
  )
}

export async function getStaticProps() {
  const dataPositivos = require("../public/data/latestPositivos.json");
  const histPositivos = require("../public/data/histPositivos.json");
  const dataFallecidos = require("../public/data/latestFallecidos.json");
  const dataChart = formatChartData()
  return {
    props: {
      dataPositivos,
      histPositivos,
      dataFallecidos,
      dataChart
    },
  };
}