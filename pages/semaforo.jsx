import Head from 'next/head'
import Header from "../components/Header";

import styles from '../styles/Semaforo.module.css'
import PeruMap from '../components/PeruMap'

export default function Semaforo(){
    return (
        <>
            <Head>
                <title>Semáforo Epidemiológico</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Header />

            <main className={styles.main}>
                <section className={styles.tracking}>
                    <span>Inicio</span>
                    <span>/</span>
                    <span>Semáforo Epidemiológico</span>
                </section>
                <h2 className={styles.titulo}>Semáforo Epidemiológico</h2>
                <section className={styles.containerSemaforo}>
                    <div className={styles.mapa}>
                        <PeruMap/>
                    </div>
                    <div className={styles.detail}>
                        <div class={styles.selector}>
                            <label><small>Seleccione una ciudad</small></label>
                            <select>
                                <option>
                                    Chiclayo, Chiclayo, Lambayeque
                                </option>
                            </select>
                        </div>
                        <h3 className={styles.status}>
                            <span className={styles.circle}></span>
                            Nivel Alto
                        </h3>

                        <div className={styles.card}>
                            <div className={styles.cardHeader}>
                                <img src="Restricciones.svg" width={30} />
                                <h5>Restricciones</h5>
                            </div>
                            <div className={styles.cardBody}>
                                <strong>Inmovilizacion Social Obligatoria</strong>
                                <small>(Puede ir a comprar una persona por familia)</small>
                                <br/>
                                <br/>
                                <strong>Transporte Interprovincial</strong>
                                <small>Terrestre y aéreo 0%</small>
                                <br/>
                                <br/>
                                <strong>Salida Peatonal</strong>
                                <small>1 hora diaria</small>
                            </div>
                        </div>

                        <div className={styles.card + " " +styles.verde}>
                            <div className={styles.cardHeader+ " "+ styles.headerVerde}>
                                <img src="Aforo.svg" width={30} />
                                <h5>Aforo</h5>
                            </div>
                            <div className={styles.cardBody}>
                                <strong>Inmovilizacion Social Obligatoria</strong>
                                <small>(Puede ir a comprar una persona por familia)</small>
                                <br/>
                                <br/>
                                <strong>Transporte Interprovincial</strong>
                                <small>Terrestre y aéreo 0%</small>
                                <br/>
                                <br/>
                                <strong>Salida Peatonal</strong>
                                <small>1 hora diaria</small>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </>
    )
}